import youTubeSearch from 'youtube-search-promise'

export default query => {
  let params ={
    maxResult: 70,
    key: 'AIzaSyC4ZKdzkbxweGwcSsU8IHOsEutAzmTJO1w'
  }
  return youTubeSearch(query, params)
}
